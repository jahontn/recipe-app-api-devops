#!/bin/bash

sudo yum update -y
sudo amazon-linuxextras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo username -aG docker ec2-user